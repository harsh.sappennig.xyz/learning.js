/* Scope:- The Scope chain is used to resolve the value of variable names in javaScript .
   Lexical scoping:- A function that is lexically within another function get access to the scope of the outer function.*/
// global scope
   var a = "hello guys...";

   function first(){
    var b = "How are you...";
    second();

    function second(){
        var c = "I am harsh Shrivastav";
        console.log(a+b+c);
    }
   }
   first();

//========================================================================================================================
/*Event Loop :- An event loop is something that pulls stuff out of the queue and places it onto the function execution stack whenever
                the function stack becomes empty */
const fun2 =()=>{
    setTimeout(()=>{
        console.log("fun2 is starting");
    },3000);
}
const fun1 =()=>{
    console.log("fun1 is starting");
    fun2();
    console.log("fun1 is starting"); 
}
fun1();

//============================================================================================================================
/*Execution Context :- JavaScript engine creates the global execution context before it starts to execute any code.
    Variables, and function that is not inside any function. A new execution context gets created every times a function is executed
    The global execution context just like any other execution context, except that it gets created by default. It is associated with
    Global Object. Which means a window object. Ex:- this === window, name == window.name*/

//=============================================================================================================================
/*Type Coercion :- Type Coercion is the automatic or implicit conversion of values from one data type to another
  FOR EX:- converting a string value to an equivalent number value. */

//=============================================================================================================================
/*pass by value :- primitive data type */
let a = 5;
let b = a;
// b = a+5

console.log(a)
console.log(b)
/*pass by reference:- non-primitive data type*/
let obj1 ={
    name: "harsh",
    pass: "shrivastav",
};
let obj2 = obj1;
// obj2.pass = "sahu";

console.log(obj1);
console.log(obj2);

//==============================================================================================================================
/*pure function :- Pure function are function that accept an input and return a value without modifying any data outside its scope.{side effects}
   side effect - dont modify any variable.*/
var x =10;
function add(x){
console.log(x+1)
}
add (10);
add(11);

//===============================================================================================================================