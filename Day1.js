/* Hosting:- Hosting is a javascript mechanism where variable and function decleration are 
   moved to the top of their scope before code execution*/

sum (5,10)
function sum(a,b){
    add = a+b;
    console.log(add)
}
// ========================================================================================================
/*asyn await:-The async and await keyword enable asynchronous, promise based behaviour to be 
  written in a cleaner style avoiding the need to explicity configure promise chains*/

  const createAuthor = async function(req, res) {
    try {
        let requestBody = req.body

        if (!isValidRequestBody(requestBody)) {
            res.status(400).send({ status: false, message: 'Invalid request parameters. Please provide author details' })
            return
        }
        if (!isValid(requestBody.name)) {
            res.status(400).send({ status: false, message: 'First name is required' })
            return
        }
        const isValidPhone = function(phone) {
            phone = phone.trim()
            let regexForphone = /^(?:(?:\+|0{0,2})91(\s*|[\-])?|[0]?)?([6789]\d{2}([ -]?)\d{3}([ -]?)\d{4})$/
            return regexForphone.test(phone)
        };
        if (!isValidPhone(requestBody.phone)) {
            res.status(400).send({ status: false, message: 'phone is required' })
            return
        }


        if (!isValid(requestBody.email)) {
            res.status(400).send({ status: false, message: 'email is required' })
            return
        }
        if (!isValid(requestBody.message)) {
            res.status(400).send({ status: false, message: 'message is required' })
            return
        }
        if (!(validator.isEmail(requestBody.email))) {
            return res.status(400).send({ status: false, msg: 'enter valid email' })
        }

        const isEmailAlreadyUsed = await authcontact.findOne({ email: requestBody.email });
        if (isEmailAlreadyUsed) {
            res.status(400).send({ status: false, message: `${requestBody.email} email address is already registered` })
            return
        }

        let createdAuthor = await authcontact.create(requestBody);
        res.status(201).send({ status: true, msg: `Contact created successfully`, data: createdAuthor });

    } catch (error) {
        res.status(500).send({ status: false, msg: error.message });
    }
};
// =========================================================================================================
/*Promises:-Promises are used to handle asynchronous operations in javascript. They are easy to manage when dealing with
            multiple asynchronous operations where callbacks can create callback hell leading to unmanageable code.
            A promise is an object that keep track about whether a certain event has happened already or not.
            And, There are three stages 1. pending, 2. fullfilled, 3. rejected
            */
// Resolve,executor
//task 1. 2s roll no; 2. 2s name and age;
const pobj1 = new Promise((resolve, reject)=>{
   setTimeout( () => {
    let roll_no = [1,2,3,4,5];
    resolve(roll_no);
   },2000)
});
//promise consume, parameter pass 
pobj1.then((rollno) =>{
    console.log(rollno);
})

//reject
const pobj2 = new Promise((resolve, reject)=>{
    setTimeout( () => {
     let roll_no = [1,2,3,4,5];
    //  resolve(roll_no);
    reject ("Error while communicating");
    },2000)
 });
 //promise consume, parameter pass 
 pobj1.then((rollno) =>{
     console.log(rollno);
 } ).catch((error)=>{
    console.log(error);
 })

